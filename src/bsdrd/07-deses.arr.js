// const persona = {
//     nombre: 'tony',
//     edad: 45,
//     clave:'ironman'
// };
// const { nombre, edad, clave}= persona;

// console.log(nombre);

const personajes = [ 'Goku', 'vegeta','trunks'];

const [, p1 ] = personajes;
console.log(p1);

const retornaArreglo = () => {
    return ['ABC',123]
};

const [letras, numeros] = retornaArreglo();

const user = (valor) => {
    return [valor, () => (console.log('holamundo'))];
};

const arr = user('goku');
console.log(arr);

// se llama a la funcion console.log del arreglo
arr[1]();

const [ nombre, setNombre] = user('goku');
console.log(nombre);
setNombre();