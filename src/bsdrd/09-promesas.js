import { getHeroeById } from './bsdrd/08-import'
// const promesa = new Promise((resolve,reject) => {
//     setTimeout( () => {
//         // resolve();
//        const heroe = getHeroeById(2);
//        resolve( heroe);
//     //    reject('no se encontreo')
//     },2000);
// });

// promesa.then( (response) =>{
//     console.log('then de la promesa');
//     console.log(response);
// })
// .catch(err => console.warn(err));

const getHeroeByIdAsync = (id) => {
    return new Promise((resolve,reject) => {
        setTimeout( () => {
            // resolve();
           const heroe = getHeroeById(id);
           if(heroe){
            resolve( heroe);
           }else{
               reject('no se encontro');
           }
           
        //    reject('no se encontreo')
        },2000);
    });

};

getHeroeByIdAsync(3).then(console.log
).catch(console.log);