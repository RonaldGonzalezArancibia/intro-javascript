// import { heroes } from './data/heroes';
import {heroes} from './../data/heroes';

const getHeroeById = (id) => {
    return  heroes.find((x) => x.id === id)
}

// console.log(getHeroeById(2));


// const getHeroesByOwner = (owner) => {
//     return  heroes.filter((x) => x.owner === owner)
// }

// console.log(getHeroesByOwner('DC'));

export {
    getHeroeById
}