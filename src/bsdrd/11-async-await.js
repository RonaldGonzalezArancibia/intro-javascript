// con promesa
// const getImagenPromesa = () => new Promise( resolve => resolve('la url de la imagen'));

// getImagenPromesa().then(console.log);

const getImagen = async () => {
    const apiKey = 'dRD17xnBg31KMQQTdvDhnTvIMwML6EKT';
    const resp = await fetch(`http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
    // con el await espera que responda para pasar a la siguente linea
    const { data } = await resp.json();
    const { url } = data.images.original;
    const img = document.createElement('img');
    img.src = url;
    document.body.append(img);
    

};

// console.log(getImagen());


getImagen().then(console.log);


// const apiKey = 'dRD17xnBg31KMQQTdvDhnTvIMwML6EKT';
// const peticion = await fetch(`http://api.giphy.com/v1/gifs/random?api_key=${apiKey}`);
// // peticion
//     .then( resp => resp.json())
//     .then(({data}) => {
//         const { url }= data.images.original;
//         const img= document.createElement('img');
//         img.src = url;
//         document.body.append( img );
//     })
//     .catch(console.warn);