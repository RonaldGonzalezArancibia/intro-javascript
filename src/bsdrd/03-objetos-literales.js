const persona = {
    nombre: 'Tony',
    apellido: 'stark',
    edad: 45,
    direccion:{
        ciudad:'santiago',
        zip: 5151155,
        lat: 14.32562,
        lng: 34.51515,
    }
};

console.log({persona:persona});
console.log({persona});
console.table(persona);

const persona2 = {...persona};